# Decent ejabberd Config

ejabberd is a great XMPP server written in erlang.
I host an XMPP server with it on a3.pm, so I can have the JID `a@a3.pm` and have all the nice XEPs.

This is the config I use for a3.pm, with small changes to remove my email and such.

It's based on default config, so all comments etc are still there.

### Disclaimer

I'm open sourcing this just because I want people to not spend hours setting up ejabberd like I did. 

I didn't spend a lot of time making this pretty or easy to use, but it's easier than digging into google and docs to find out what you need to do.

### Results

Test was done with [ComplianceTester](https://github.com/iNPUTmice/ComplianceTester).

```
Use compliance suite 'Conversations Compliance Suite' to test a3.pm
Server is ejabberd 18.4.0
running XEP-0115: Entity Capabilities…          PASSED
running XEP-0163: Personal Eventing Protocol…           PASSED
running Roster Versioning…              PASSED
running XEP-0280: Message Carbons…              PASSED
running XEP-0191: Blocking Command…             PASSED
running XEP-0045: Multi-User Chat…              PASSED
running XEP-0198: Stream Management…            PASSED
running XEP-0313: Message Archive Management…           PASSED
running XEP-0352: Client State Indication…              PASSED
running XEP-0363: HTTP File Upload…             PASSED
running XEP-0065: SOCKS5 Bytestreams (Proxy)…           PASSED
running XEP-0357: Push Notifications…           PASSED
running XEP-0368: SRV records for XMPP over TLS…                FAILED
running XEP-0384: OMEMO Encryption…             PASSED
running XEP-0313: Message Archive Management (MUC)…             PASSED
passed 14/15
```

### Usage

This is designed to be used with [the docker container](https://hub.docker.com/r/ejabberd/ecs/), but you should be able to edit it so that it can run outside docker too.

Firstly, you'll need to get an SSL cert.     
I got one for these domains in my case:

```
x.a3.pm
conference.x.a3.pm
pubsub.x.a3.pm
echo.x.a3.pm
irc.x.a3.pm
upload.x.a3.pm
a3.pm
conference.a3.pm
pubsub.a3.pm
echo.a3.pm
irc.a3.pm
upload.a3.pm
```

(they're repeated because while server is on x.a3.pm, using SRV records I got XMPP accounts on a3.pm)

If you use certbot to do this, it'll give out two pem files. Combine them into one file, then pass it into docker.

After this, if you just want one domain, changing the lines that have `# CHANGE_THIS` should be enough. If not, you'll have to mess around a bit.

You'll need to open the ports 5222, 5269, 5280 and 5443 and pass this config file in, and then you'll need to run the ejabberd container.

Registers are disabled by default, and you'll have to mess around to enable that.

If you want to add people manually, use this command: `docker exec -it ejabberd bin/ejabberdctl register <name> <domain> <password>`. As we support the in-band registration XEP (which provides registration, password change and account deletion capabilities) but have registrations disabled, people will be able to change the password to the password they want.

Have fun!